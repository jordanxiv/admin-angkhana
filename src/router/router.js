import Vue from "vue";
import Router from "vue-router";
import auth from "@/middleware/auth";
import guest from "@/middleware/guest";
import middlewarePipeline from "./middlewarePipeline";
import { store } from "@/store/index";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "*",
      redirect: {
        name: "home"
      }
    },
    {
      path: "/",
      component: () => import("@/layouts/default.vue"),
      children: [
        {
          name: "home",
          path: "/",
          component: () => import("@/pages/index.vue")
        },
        {
          name: "categories",
          path: "/categories",
          component: () => import("@/pages/categories.vue")
        }
      ]
    },
    {
      path: "/category",
      component: () => import("@/layouts/default.vue"),
      children: [
        {
          name: "category-id",
          path: ":id",
          component: () => import("@/pages/category/_id.vue")
        }
      ]
    },
    {
      path: "/",
      component: () => import("@/layouts/none.vue"),
      children: [
        {
          name: "login",
          path: "login",
          component: () => import("@/pages/login.vue"),
          meta: {
            middleware: [guest]
          }
        },
        {
          name: "register",
          path: "register",
          component: () => import("@/pages/register.vue"),
          meta: {
            middleware: [guest]
          }
        }
      ]
    },
  ]
});

router.beforeEach((to, from, next) => {
  if (!to.meta.middleware) {
    return next();
  }
  const middleware = to.meta.middleware;

  const context = {
    to,
    from,
    next,
    store
  };

  return middleware[0]({
    ...context,
    next: middlewarePipeline(context, middleware, 1)
  });
});

export default router;
