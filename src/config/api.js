import endpoint from "../config/endpoint";
import axios from "axios";

const parseResponse = response => {
  return response.data;
};

// import { withToken } from "../config/getHeader";
// let token = localStorage.getItem('token')
export const api = {
  register: data =>
    axios({
      method: "post",
      url: endpoint.register,
      data: data
    }).then(parseResponse),

  login: data =>
    axios({
      method: "post",
      url: endpoint.login,
      data: data
    }).then(parseResponse),

  logout: ({ code }, token) =>
    axios({
      method: "post",
      url: endpoint.logout,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: token
      },
      data: {
        code: code
      }
    }).then(parseResponse),

  getAllCategories: () =>
    axios({
      method: "post",
      url: endpoint.getAllCategories,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }).then(parseResponse),
    
  getCategoryById: id =>
    axios({
      method: "post",
      url: endpoint.getCategoryById,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      data: { id: id }
    }).then(parseResponse)
};
