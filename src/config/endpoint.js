export default {
  register: "/register",
  login: "/login",
  logout: "/logout",

  // CATEGORY
  getAllCategories: "/get-categories",
  getCategoryById: "/get-category"
};
