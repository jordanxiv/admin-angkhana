module.exports = {
  pluginOptions: {
    autoRouting: {
      chunkNamePrefix: "page-"
    },
    env: {
      APP_NAME: "Angkhana adminustration",
      API_URL: "http://localhost:3000/api"
    }
  },
  transpileDependencies: ["vuetify"]
};
